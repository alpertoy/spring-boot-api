package io.gitlab.alpertoy.clientservice.repositories;

import io.gitlab.alpertoy.clientservice.entities.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
