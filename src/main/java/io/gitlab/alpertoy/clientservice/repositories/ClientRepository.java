package io.gitlab.alpertoy.clientservice.repositories;

import io.gitlab.alpertoy.clientservice.entities.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
}
